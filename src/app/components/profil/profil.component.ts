import { Component, OnInit } from '@angular/core';
import { OrderService } from 'src/app/services/order.service';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css'],
})
export class ProfilComponent implements OnInit {
  listorder: any;

  userconnect = JSON.parse(localStorage.getItem('userconnect')!);

  constructor(private orderservice: OrderService) {}

  ngOnInit(): void {
    this.filterliste()
  }

  /* getallorder() {
    this.orderservice.allorder().subscribe((res: any) => {
      this.listorder = res['data'];
      console.log('product : ', this.listorder);
    });
  } */

  filterliste(){
    
      this.orderservice.allorder().subscribe((res: any) => {
        this.listorder = res['data'].filter(
          (element: any) =>
            element.client._id == this.userconnect._id
        );
        console.log('list order :', this.listorder);
      });
    
  }
}
