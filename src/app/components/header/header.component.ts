import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  items = [] as any;
  // totalPrice = 0;
  isLogin: any;
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);

  constructor(private cartservice: CartService) {}

  ngOnInit(): void {
    this.cartservice.loadCart();
    this.items = this.cartservice.getItems();
    this.calcul();
    this.isconnect();
  }

  /* isconnect() {
    if (!this.userconnect) {
      this.isLogin = true;
    } else {
      this.isLogin = false;
    }
  } */

  isconnect(){
    return localStorage.getItem('state')=="0" ? true:false;
  }

  logout() {
    localStorage.clear();
  }

  /* calcul() {
    
    for (var i = 0; i < this.items.length; i++) {
      this.totalPrice =
        this.totalPrice +
        JSON.parse(this.items[i].prix) * JSON.parse(this.items[i].quantite);
    }
    return this.totalPrice;
  } */

  // 2eme methode :

  calcul() {
    let total = 0;
    this.items.forEach((element: any) => {
      total += Number(element.prix) * Number(element.quantite);
    });
    //this.totalPrice = total;
    return total;
  }

  deleteItem(item: any) {
    this.cartservice.removeItem(item);
    this.items = this.cartservice.getItems();
  }
}
