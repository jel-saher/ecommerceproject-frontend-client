import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from 'src/app/services/login.service';
import { RegisterService } from 'src/app/services/register.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  listuser: any;
  x: any;

  constructor(
    private formBuilder: FormBuilder,
    private registerservice: RegisterService,
    private loginservice: LoginService
  ) {}

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group(
      {
        email: ['', [Validators.required, Validators.email]],
        password: ['', Validators.required],
        /* confirmPassword: ['', Validators.required], */
      }
      /* {
        validator: MustMatch('password', 'confirmPassword'),
      } */
    );
    this.getalluser();
  }

  get f() {
    return this.registerForm.controls;
  }

  getalluser() {
    this.loginservice.getalluser().subscribe((res: any) => {
      this.listuser = res['data'];
    });
  }

  onSubmit() {
    this.submitted = true;

    for (var i = 0; i < this.listuser.length; i++) {
      if (this.registerForm.value.email === this.listuser[i].email) this.x = 1;
    }

    this.registerservice.register(this.registerForm.value).subscribe(
      (res: any) => {
        if ((res.status = 200)) {
          Swal.fire({ icon: 'success', title: 'Your Account is created' });
        }
      },
      (err) => {
        if (this.x === 1) {
          Swal.fire({ icon: 'error', title: 'Email already used' });
        } else {
          Swal.fire({ icon: 'error', title: 'Your Account is not created' });
        }
      }
    );
  }

  onReset() {
    this.submitted = false;
    this.registerForm.reset();
  }
}
