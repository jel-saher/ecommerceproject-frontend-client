import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CartService } from 'src/app/services/cart.service';
import { ProductService } from 'src/app/services/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-detailproduct',
  templateUrl: './detailproduct.component.html',
  styleUrls: ['./detailproduct.component.css'],
})
export class DetailproductComponent implements OnInit {
  list: any;
  id = this.activateroute.snapshot.params['id'];
  x = 0;
  items = [] as any;

  constructor(
    private productservice: ProductService,
    private activateroute: ActivatedRoute,
    private cartservice: CartService
  ) {}

  ngOnInit(): void {
    this.getoneproduct();
  }

  getoneproduct() {
    this.productservice.getOneProduct(this.id).subscribe((res: any) => {
      this.list = res['data'];
      console.log('Detail :  ', this.list);
    });
  }

  addproduct() {
    if (this.x < this.list.quantite) {
      this.x = this.x + 1;
    }
  }

  deleteproduct() {
    if (this.x > 0) this.x = this.x - 1;
  }

  addToCart(item: any) {
    if (!this.cartservice.itemInCart(item)) {
      item.quantite = this.x;
      this.cartservice.addToCart(item);
      this.items = this.cartservice.getItems();
      Swal.fire({
        icon: 'success',
        title: 'Product add',
      });
    }
  }
}
