import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CartService } from 'src/app/services/cart.service';
import { ProductService } from 'src/app/services/product.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css'],
})
export class CheckoutComponent implements OnInit {
  items = [] as any;
  orderForm: FormGroup;
  prixTot = 0;
  qttotal = 0;

  userconnect = JSON.parse(localStorage.getItem('userconnect')!);

  constructor(
    private cartservice: CartService,
    private route: Router,
    private formBuilder: FormBuilder,
    private productservice: ProductService
  ) {}

  ngOnInit(): void {
    this.items = this.cartservice.getItems();
    this.calcul();
    this.calculquantite();
    console.log('user :', this.userconnect);

    this.orderForm = this.formBuilder.group({
      quantite: ['', Validators.required],
      prixTotale: ['', Validators.required],
      client: ['', Validators.required],
      itemorder: ['', Validators.required],
      paye: [''],
    });
  }

  calcul() {
    let total = 0;
    this.items.forEach((element: any) => {
      total += Number(element.prix) * Number(element.quantite);
      this.prixTot = total;
     
    });
    return total;
  }

  calculquantite() {
    if (!this.userconnect){
      Swal.fire('User not connected');
    }
    
    let totalquantite = 0;
    this.items.forEach((element: any) => {
      totalquantite += Number(element.quantite);
      this.qttotal = totalquantite;
    });
    return totalquantite;
  }

  addorder() {
    if (!this.userconnect){
      Swal.fire('User not connected');
    }
    else{
    console.log('aaaaaaaaaaaaaaaa', this.items);
    this.orderForm.patchValue({
      client: this.userconnect._id,
      prixTotale: this.prixTot,
      itemorder: this.items,
      quantite: this.qttotal,
      paye: true,
      // date :new Date().toISOString().split('|')[0].toString()
    });

    this.productservice.addorder(this.orderForm.value).subscribe((res: any) => {
      console.log(res);
      Swal.fire('order added');
      this.route.navigateByUrl('/facture')
    });
    // this.route.navigateByUrl("/listproduct")
  }
}
}
