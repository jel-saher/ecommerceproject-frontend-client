import { Component, OnInit } from '@angular/core';
import { CategoryService } from 'src/app/services/category.service';
import { ProductService } from 'src/app/services/product.service';
import { Options } from 'ng5-slider';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-listproduct',
  templateUrl: './listproduct.component.html',
  styleUrls: ['./listproduct.component.css'],
})
export class ListproductComponent implements OnInit {
  listproduct: any;
  listcolor: any;
  listcategory: any;
  /* filterForm: FormGroup; */

  priceSelection = '';
  maxValue = 5000;
  minValue = 0;
  options: Options = {
    floor: 0,
    ceil: 5000,
  };

  constructor(
    private productservice: ProductService,
    private categoryservice: CategoryService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.getallproduct();
    this.getallcategory();

    /* this.filterForm = this.formBuilder.group({
      maxValue: ['', Validators.required],
      minValue: ['', Validators.required],
    }); */
  }

  getallproduct() {
    this.productservice.allproduct().subscribe((res: any) => {
      this.listproduct = res['data'];
      console.log('product : ', this.listproduct);
    });
  }
  getallcategory() {
    this.categoryservice.getallcategory().subscribe((res: any) => {
      this.listcategory = res['data'];
    });
  }
  OnChangecolor(event: any) {
    console.log('detected value color :', event.target.value);
    this.productservice.allproduct().subscribe((res: any) => {
      this.listproduct = res['data'].filter(
        (element: any) => element.color == event.target.value
      );
      console.log('list product color :', this.listproduct);
    });
  }

  OnChangeCategory(event: any) {
    this.productservice.allproduct().subscribe((res: any) => {
      this.listproduct = res['data'].filter(
        (element: any) =>
          element.subcategory.category.name == event.target.value
      );
      console.log('list product category :', this.listproduct);
    });
  }

  OnChangePrice(event: any) {
    this.productservice.allproduct().subscribe((res: any) => {
      this.listproduct = res['data'].filter(
        (element: any) => element.prix == event.target.value
      );
      console.log('list product category :', this.listproduct);
    });
  }

  changePrice() {
    console.log('Price change :', this.priceSelection);
    let event = this.priceSelection;
    this.productservice.allproduct().subscribe((res: any) => {
      this.listproduct = res['data'];
      if (event !== undefined) {
        const listproductbyprice = this.listproduct.filter(
          (element: any) => element.prix >= event[0] && element.prix <= event[1]
        );
        this.listproduct = listproductbyprice;
        console.log('filter by price : ', this.listproduct);
      }
    });
  }

  /* filterPrice() {
   let  min = this.filterForm.value.minValue;
   let  max = this.filterForm.value.maxValue
    console.log('min :', min,"max : ",max);
    this.productservice.allproduct().subscribe((res: any) => {
      this.listproduct = res['data'];
      if (min !== undefined && max !== undefined ) {
        const listbyprice = this.listproduct.filter(
          (element: any) => element.prix >= min && element.prix <= max
        );
        this.listproduct = listbyprice;
        console.log('filter min max : ', this.listproduct);
      }
    });
  } */
}
