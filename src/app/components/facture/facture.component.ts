import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/services/cart.service';
import jsPDF from 'jspdf';
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-facture',
  templateUrl: './facture.component.html',
  styleUrls: ['./facture.component.css']
})
export class FactureComponent implements OnInit {
  items = [] as any;
  userconnect = JSON.parse(localStorage.getItem('userconnect')!);
  prixTot = 0;

  constructor(private cartservice : CartService) { }

  ngOnInit(): void {
    this.items = this.cartservice.getItems();
    this.calcul();
    this.facture();
  }

  public openPDF(): void {
    let DATA: any = document.getElementById('htmlData');
    html2canvas(DATA).then((canvas) => {
      let fileWidth = 208;
      let fileHeight = (canvas.height * fileWidth) / canvas.width;
      const FILEURI = canvas.toDataURL('image/png');
      let PDF = new jsPDF('p', 'mm', 'a4');
      let position = 0;
      PDF.addImage(FILEURI, 'PNG', 0, position, fileWidth, fileHeight);
      PDF.save('facture_ecommerce.pdf');
    });
  }

  calcul() {
    let total = 0;
    this.items.forEach((element: any) => {
      total += Number(element.prix) * Number(element.quantite);
      this.prixTot = total;
     
    });
    return total;
  }

  facture() {
    let facture = 0;
    facture = (this.calcul()/100)*80 + (this.calcul()/100)*10;
    return facture;
  }



}
