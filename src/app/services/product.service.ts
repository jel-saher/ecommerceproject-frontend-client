import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ProductService {
  constructor(private http: HttpClient) {}

  allproduct(){
    return this.http.get(`${environment.baseurl}/product/getallproduct`)
  }

  getOneProduct(id: any) {
    return this.http.get(`${environment.baseurl}/product/getbyidproduct/${id}`);
  }

  addorder(order:any){
    return this.http.post(`${environment.baseurl}/order/createorder`,order);
  }
}
