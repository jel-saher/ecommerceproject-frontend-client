import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';



import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { LayoutComponent } from './components/layout/layout.component';
import { ListproductComponent } from './components/listproduct/listproduct.component';
import { DetailproductComponent } from './components/detailproduct/detailproduct.component';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegisterComponent } from './components/register/register.component';
import { CheckoutComponent } from './components/checkout/checkout.component';
import { FactureComponent } from './components/facture/facture.component';
import { ProfilComponent } from './components/profil/profil.component';
import { Ng5SliderModule } from 'ng5-slider';
import { QuantityproductDirective } from './directives/quantityproduct.directive';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    LayoutComponent,
    ListproductComponent,
    DetailproductComponent,
    LoginComponent,
    RegisterComponent,
    CheckoutComponent,
    FactureComponent,
    ProfilComponent,
    QuantityproductDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    Ng5SliderModule
    

    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
